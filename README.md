# vim-vpro

vim-vpro is plugin extention of [vpro](https://gitlab.com/littlesoftware/vpro) tool.

## Install plugin

In vim-plug add
```
Plug 'https://gitlab.com/littlesoftware/vim-vpro.git'
```

## Feauters

[ ] Run any target with command vpro
[ ] Add bindkey for vpro for run <leader><keys of vpro><key of target>

