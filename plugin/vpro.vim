"{{{ Global initialization
if exists('g:vpro_loaded')
  finish
endif
"}}}

"{{{ Global and local variables
let g:vpro_loaded = 1
let s:last_target=v:null
"}}}

"{{{ Common functions
func! s:error_msg(str) abort
  echoerr a:str
  throw a:str
endfunc
"}}}

"{{{ Vpro functions
func! s:find_project() abort
  return trim(system('vpro --path'))
endfunc

func! s:get_project_targets() abort
  let l:info = json_decode(system('vpro -fjson --list'))
  if l:info.type != 'targets'
    error_msg('Invalid type of targets list')
  endif
  return l:info.list
endfunc

func! s:get_target_details(target) abort
  let l:details = json_decode(system('vpro -fjson --details ' .. a:target))
  if l:details.type != 'target details'
    error_msg('Invalid type of target details')
  endif
  return #{
        \ cwd: l:details.cwd,
        \ lets: l:details.lets,
        \ commands: l:details.commands
        \ }
endfunc

func! s:run_debug(target) abort
  let l:details = s:get_target_details(a:target)
  let l:debug_type = l:details.lets.debug
  let l:debug_cwd = l:details.cwd
  let l:debug_command = l:details.commands[0]
  let l:debug_program = l:debug_command[0]
  if l:debug_type != 'cpp'
    error_msg('Unsupported debug type')
  endif

  call vimspector#LaunchWithConfigurations( #{
        \ launch: #{
        \   adapter: 'vscode-cpptools',
        \   configuration: #{
        \     request: 'launch',
        \     program: l:debug_program,
        \     externalConsole: v:true
        \   }
        \ }
        \ })
endfunc

func! s:run_target(target) abort
  let l:opt = #{
    \vertical: 1
  \}
  let l:buf = term_start(['vpro', a:target], l:opt)
  let l:job = term_getjob(l:buf)
  nnoremap <buffer> <silent> <cr> :bw<cr>
endfunc
"}}}

"{{{ Vpro plugin API
func! s:vpro_run_target(reset)
  if a:reset || s:last_target is v:null
    let l:targets = s:get_project_targets()
    let l:res = fzf#run({'source': l:targets, 'sink': function('s:run_target')})
    if len(l:res) > 0
      let s:last_target = l:res[0]
    endif
  else
    call s:run_target(s:last_target)
  endif
endfunc

func! s:vpro_run_debug()
  let l:targets = s:get_project_targets()
  let l:res = fzf#run({'source': l:targets, 'sink': function('s:run_debug')})
endfunc
"}}}

"{{{ Vpro mapping
nnoremap <silent> <Plug>VproRunTargetRepeat :<C-U>call <SID>vpro_run_target(v:false)<CR>
nnoremap <silent> <Plug>VproRunTargetReset :<C-U>call <SID>vpro_run_target(v:true)<CR>
nnoremap <silent> <Plug>VproRunTargetDebug :<C-U>call <SID>vpro_run_debug()<CR>

if !exists("g:vpro_no_mappings") || ! g:vpro_no_mappings
  nnoremap <leader>rr <Plug>VproRunTargetRepeat
  nnoremap <leader>rt <Plug>VproRunTargetReset
  nnoremap <leader>rd <Plug>VproRunTargetDebug
endif
"}}}
